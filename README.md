# Yalantis Ruby School test project

### Завдання:
- створити простий Rails додаток

### Задачі:
1) створити новий Rails додаток delivery_app
2) створити модель Courier з наступними полями:
    - name: String
    - email: String
3) створити сторінки для створення, відображення та зміни кур'єра
4) створити сторінку зі списком кур'єрів та посиланнями на дії над кур'єром (з попереднього пункту)
5) створити модель Package з наступними полями:
    - tracking_number: Sting
    - delivery_status: Boolean
6) створити зв'язок між Courier і Package за принципом один до багатьох
7) додати до сторінки відображення кур'єра форму для створення посилки

---
Для запуску виконати `rails server`
