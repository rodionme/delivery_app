# frozen_string_literal: true

#:nodoc: all
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
