# frozen_string_literal: true

# Represents a single package.
class Package < ApplicationRecord
  belongs_to :courier
end
