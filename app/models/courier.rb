# frozen_string_literal: true

# Represents a single courier.
class Courier < ApplicationRecord
  has_many :packages, dependent: :destroy
  validates :name, presence: true, length: { minimum: 5 }
end
